// bash2fish_test
package main

import (
	"strings"
	"testing"
)

func Test_convert_line(t *testing.T) {
	func_open := false
	default_replacer := strings.NewReplacer("&&", "; and ", "||", "; or ", " !",
		" not ", "unset ", "set --erase ", "$@", "$argv", "$#", "(count $argv)",
		"(", "", ")", "")
	export_replacer := strings.NewReplacer("export ", "set --export ", "=", " ", ":", " ")
	func_open_replacer := strings.NewReplacer("{", "")
	func_close_replacer := strings.NewReplacer("}", "end")
	actual := convert_line("alias testalias='foo'", &func_open,
		default_replacer, export_replacer, func_open_replacer, func_close_replacer)
	expected := "alias testalias='foo'"
	if actual != expected {
		t.Error("Simple alias test failed: ", actual)
	}

	actual = convert_line("alias testalias='foo&&bar'", &func_open,
		default_replacer, export_replacer, func_open_replacer, func_close_replacer)
	expected = "alias testalias='foo; and bar'"
	if actual != expected {
		t.Error("'&&' test failed: ", actual)
	}

	actual = convert_line("alias testalias='foo||bar'", &func_open,
		default_replacer, export_replacer, func_open_replacer, func_close_replacer)
	expected = "alias testalias='foo; or bar'"
	if actual != expected {
		t.Error("'||' test failed: ", actual)
	}

	actual = convert_line("alias testalias=' !foo||bar'", &func_open,
		default_replacer, export_replacer, func_open_replacer, func_close_replacer)
	expected = "alias testalias=' not foo; or bar'"
	if actual != expected {
		t.Error("' !' test failed: ", actual)
	}

	actual = convert_line("export foo=bar", &func_open,
		default_replacer, export_replacer, func_open_replacer, func_close_replacer)
	expected = "set --export foo bar"
	if actual != expected {
		t.Error("'export =' test failed: ", actual)
	}

	actual = convert_line("export foo='bar:baz'", &func_open,
		default_replacer, export_replacer, func_open_replacer, func_close_replacer)
	expected = "set --export foo 'bar baz'"
	if actual != expected {
		t.Error("'export :' test failed: ", actual)
	}

	actual = convert_line("unset foo", &func_open,
		default_replacer, export_replacer, func_open_replacer, func_close_replacer)
	expected = "set --erase foo"
	if actual != expected {
		t.Error("'unset' test failed: ", actual)
	}
}

func Test_convert_linearr(t *testing.T) {
	actual := convert_linearr([]string{"function foo() {", "bar $@", "}"})
	expected := []string{"function foo", "bar $argv", "end"}
	if arr_comp(actual, expected) {
		t.Error("$@ function test failed")
	}

	actual = convert_linearr([]string{"function foo() {", "bar $#", "}"})
	expected = []string{"function foo", "bar (count $argv)", "end"}
	if arr_comp(actual, expected) {
		t.Error("$# function test failed")
	}
}

//check if string arrays a and b are same
func arr_comp(a, b []string) bool {
	if len(a) != len(b) {
		return false
	} else {
		for i := 0; i < len(a); i++ {
			if a[i] != b[i] {
				return false
			}
		}
		return true
	}
}
