// bash2fish
package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	args := os.Args
	if len(args) == 1 {
		fmt.Println("bash2fish: The bash-to-fish configuration converter written in Go")
		fmt.Println("Please give a single bash configuration/aliases file and any desired arguments:")
		fmt.Println("\t-sudofix\tAppend a function to simulate \"sudo !!\"[default]")
		fmt.Println("\t-nosudofix\tDon't append a function to simulate \"sudo !!\"")
		return
	}
	//Whether to append a function to emulate "sudo !!"
	sudofix := true
	filename := ""
	for i := 1; i < len(args); i++ {
		switch args[i] {
		case "-sudofix":
			sudofix = true
		case "-nosudofix":
			sudofix = false
		default:
			filename = args[i]
		}
	}
	fmt.Println("Processing File: ", filename)
	linearr := loadfile(filename)
	converted_linearr := convert_linearr(linearr)
	if sudofix {
		converted_linearr = append(converted_linearr,
			"function sudo\n"+
				"\tif test \"$argv\" = !!\n"+
				"\t\teval command sudo $history[1]\n"+
				"\telse\n"+
				"\t\tcommand sudo $argv\n"+
				"\tend\n"+
				"end")
	}
	fmt.Println("Writing to file config.fish")
	writefile(converted_linearr)
}

func loadfile(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		panic("Bad filename give")
	}
	defer file.Close()
	var linearr []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		linearr = append(linearr, scanner.Text())
	}
	return linearr
}

func writefile(linearr []string) error {
	filename := "config.fish"
	file, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	for _, line := range linearr {
		fmt.Fprintln(w, line)
	}
	return w.Flush()
}

func convert_linearr(linearr []string) (outarr []string) {
	func_open := false
	default_replacer := strings.NewReplacer("&&", "; and ", "||", "; or ", " !",
		" not ", "unset ", "set --erase ", "$@", "$argv", "$#", "(count $argv)",
		"(", "", ")", "")
	export_replacer := strings.NewReplacer("export ", "set --export ", "=", " ", ":", " ", "\"", "")
	func_open_replacer := strings.NewReplacer("{", "")
	func_close_replacer := strings.NewReplacer("}", "end")
	for i := range linearr {
		outarr = append(outarr, convert_line(linearr[i], &func_open,
			default_replacer, export_replacer, func_open_replacer, func_close_replacer))
	}
	return outarr
}

func convert_line(line string, func_open *bool, default_replacer, export_replacer,
	func_open_replacer, func_close_replacer *strings.Replacer) (outline string) {
	if len(line) > 0 && line[0] != '#' {
		//replace the logical operators
		outline = default_replacer.Replace(line)
		if strings.Contains(outline, "export") {
			outline = export_replacer.Replace(outline)
		}
		//reformat functions. Fish does not use brackets, but rather opens
		//and closes the function with "function" and "end"
		if strings.Contains(outline, "function") {
			outline = func_open_replacer.Replace(outline)
			*func_open = true
		}
		if *func_open {
			if strings.Contains(outline, "}") {
				outline = func_close_replacer.Replace(outline)
				*func_open = false
			}
		}
	} else {
		outline = line
	}
	return outline
}
