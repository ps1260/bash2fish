# bash2fish
A bash_aliases to fish.conf converter

A simple tool for converting bash aliases and functions into the fish format, 
written in Go. It works for my simple .bash_aliases file, but by no means is 
capable of converting all constructs into a valid config.fish file.

If you prefer Rust, try the [bash2fish_rs](https://gitlab.com/ps1260/bash2fish_rs), 
try my rust clone of this utility.
